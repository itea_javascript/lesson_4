/*
  ASYNC-AWAIT

  Внутри функции которая помеченая как async в можете поместить
  ключевое слово await перед выражением которое вернет promise.
  Во время выполнения асинхронной функции она останавливается пока
  промис не станет выполненным.

  Задача конструкции async-await писать асинхронный код, который
  будет читаться так же просто как и синхронный
*/


/* jshint ignore:start */


  // Game
  async function PlayGame(){
    let rand = Math.floor(  Math.random() *  100 );
    rand % 2 === 0 ? rand = true : rand = false;
    return rand;
  }

  let GameResult = PlayGame();
  console.log(GameResult);
  GameResult.then( res => res === true ? console.log('YOU WON') : console.log('YOU LOSE'));


// Demo with Response Awaiting
  function resolveAfter( number, ms) {
    return new Promise(
      resolve => {
        setTimeout(() => {
          resolve( number );
        }, ms);
      });
  }

  async function CombineNumbers(){
    let a = await resolveAfter( 20, 1000 );
    let b = await resolveAfter( 40, 1000 );
    return a + b;
  }

  let sixteen = CombineNumbers();
  sixteen.then( res => console.log( 'res', res ));

  // Combine User
  async function getUserWithFriends(){
    const getUserResponse = await fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2");
    const users = await getUserResponse.json();

    const selectedUser = users[0];
    const selectedUserName = selectedUser.name;
    const getUserFriends = await fetch("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2");
    const UserFriends = await getUserFriends.json();

    const CombinedUser = {
      name: selectedUserName,
      friends: UserFriends[0].friends
    };
    return CombinedUser;

  }
  var UserWithFriends = getUserWithFriends();
      UserWithFriends.then( data => console.log('data', data));

/* jshint ignore:end */
// DEMO FROM PROMISE
// fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2").then(
//   resolve = ( res ) => {
//     return res.json();
//   }
// ).then(
//   (data) => {
//     return data[0];
//   }
// ).then(
//   (data) => {
//     console.log('data', data);
//     return data.name;
//   }
// ).then(
//   (data) => {
//
//     // show friends
//     return fetch("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2").then(
//         (res) => res.json()
//       ).then( dataJson => {
//         return {
//           name: data,
//           friends: dataJson[0]
//         };
//       });
//
//   }
// ).then(
//   (after2Fetch) =>{
//     console.log(after2Fetch);
//   }
// )
// .catch(error => {
//   console(error); // Error: Not Found
// });
