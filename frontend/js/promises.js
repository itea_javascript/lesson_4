/*
  PROMISES
*/

  // PROMISES
  // Promise status : Pending | Fulfilled | Rejected

  /* FIRST PROMISE
   let TestPromise = new Promise( function( resolve, reject ){
    setTimeout( () => {
      // переведёт промис в состояние fulfilled с результатом "result"
      // resolve("result");
      // reject('ERROR 404');
    }, 1000);
  });
      TestPromise.then(
    res => {
      console.log('Fulfilled: ' + res );
    },
    error => {
      throw new Error('Rejected: ' + error);
    }
  );
      console.log( TestPromise );
  */

  /* ASYNC PROMISE
    fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2").then(
      resolve = ( res ) => {
        return res.json();
      }
    ).then(
      (data) => {
        return data[0];
      }
    ).then(
      (data) => {
        console.log('data', data);
        return data.name;
      }
    ).then(
      (data) => {

        // show friends
        return fetch("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2").then(
            (res) => res.json()
          ).then( dataJson => {
            return {
              name: data,
              friends: dataJson[0]
            };
          });

      }
    ).then(
      (after2Fetch) =>{
        console.log(after2Fetch);
      }
    )
    .catch(error => {
      console(error); // Error: Not Found
    });
  */

  /* LOAD CAT PROMISE */

  function loadImagePromise( url ){
    return new Promise( (resolve, reject) => {

      let imageElement = new Image();
          imageElement.onload = function(){
            resolve( imageElement );
          };
          imageElement.onerror = function(){
            let message = 'Error on image load at url ' + url;
            reject(
              RenderImage('images/cat5.jpg')
            );
          };

          imageElement.src = url;
    });
  }
  loadImagePromise('asdasd');
  // loadImagePromise('images/cat1.jpg').then((img) => {
  //   console.log( img );
  //   RenderImage(img.src);
  // });

  // Promise.all([
  //   loadImagePromise('images/cat1.jpg'),
  //   loadImagePromise('images/cat2.jpg'),
  //   loadImagePromise('images/cat3.jpg'),
  //   loadImagePromise('images/cat4.jpg'),
  //   loadImagePromise('images/cat5.jpg'),
  //   loadImagePromise('images/cat6.jpg'),
  //   loadImagePromise('images/cat7.jpg')
  // ]).then( images => {
  //   images.forEach( img => RenderImage( img.src ));
  // }).catch( error => new Error( error ));






















// Простая функция которая в блоке с id tagert отрисовывает нам картинку с указаным урл.
let RenderImage = (src) => {
  let ImageElement = document.createElement("img");
      ImageElement.src = src;

  let TargetBlock = document.getElementById("target");
      TargetBlock.appendChild(ImageElement);
};
